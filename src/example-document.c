#include "example-document.h"
#include "example-editor-search-bar.h"

struct _ExampleDocument
{
  GtkTextBuffer parent_instance;
  gchar *title;
  GtkOverlay    *overlay;
  GtkRevealer   *search_revealer;
  ExampleEditorSearchBar *search_bar;
  GtkSourceView *source_view;
  GtkSearchEntry *search;
};

enum {
  PROP_0,
  PROP_TITLE,
  PROP_SEARCH,
  N_PROPS
};

G_DEFINE_TYPE (ExampleDocument, example_document, GTK_TYPE_TEXT_BUFFER)

static guint last_untitled;
static GParamSpec *properties [N_PROPS];

static void
example_editor_page_stop_search (ExampleDocument      *self,
                             ExampleEditorSearchBar *search_bar)
{
  g_assert (EXAMPLE_IS_DOCUMENT (self));
  g_assert (EXAMPLE_IS_EDITOR_SEARCH_BAR (search_bar));

  gtk_revealer_set_reveal_child (self->search_revealer, FALSE);
  gtk_widget_grab_focus (GTK_WIDGET (self->source_view));

}

static void
example_editor_page_notify_child_revealed (ExampleDocument *self,
                                       GParamSpec    *pspec,
                                       GtkRevealer   *revealer)
{
  g_assert (EXAMPLE_IS_DOCUMENT (self));
  g_assert (GTK_IS_REVEALER (revealer));

  if (gtk_revealer_get_child_revealed (revealer))
    {
      GtkWidget *toplevel = gtk_widget_get_ancestor (GTK_WIDGET (revealer), GTK_TYPE_WINDOW);
      GtkWidget *focus = gtk_window_get_focus (GTK_WINDOW (toplevel));

      /* Only focus the search bar if it doesn't already have focus,
       * as it can reselect the search text.
       */
      if (focus == NULL || !gtk_widget_is_ancestor (focus, GTK_WIDGET (revealer)))
        gtk_widget_grab_focus (GTK_WIDGET (self->search_bar));
    }
}



static void
example_document_constructed (GObject *object)
{
  ExampleDocument *self = (ExampleDocument *)object;

  if (self->title == NULL)
    self->title = g_strdup_printf ("Untitled Document %u", ++last_untitled);

  G_OBJECT_CLASS (example_document_parent_class)->constructed (object);
}

static void
example_document_finalize (GObject *object)
{
  ExampleDocument *self = (ExampleDocument *)object;

  g_clear_pointer (&self->title, g_free);

  G_OBJECT_CLASS (example_document_parent_class)->finalize (object);
}

static void
example_document_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  ExampleDocument *self = EXAMPLE_DOCUMENT (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_set_string (value, self->title);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
example_document_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  ExampleDocument *self = EXAMPLE_DOCUMENT (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_free (self->title);
      self->title = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
example_document_class_init (ExampleDocumentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = example_document_finalize;
  object_class->constructed = example_document_constructed;
  object_class->get_property = example_document_get_property;
  object_class->set_property = example_document_set_property;

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "The title of the document",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);


  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/example/ui/example-document.ui");
  gtk_widget_class_bind_template_child (widget_class, ExampleDocument, overlay);
  gtk_widget_class_bind_template_child (widget_class, ExampleDocument, search_bar);
  gtk_widget_class_bind_template_child (widget_class, ExampleDocument, search_revealer);
  gtk_widget_class_bind_template_callback (widget_class, example_editor_page_notify_child_revealed);
  gtk_widget_class_bind_template_callback (widget_class, example_editor_page_stop_search);

  g_type_ensure (EXAMPLE_TYPE_EDITOR_SEARCH_BAR);
}

static void
example_document_init (ExampleDocument *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

ExampleDocument *
example_document_new (GObject *object)
{
  ExampleDocument *self = (ExampleDocument *)object;
  example_editor_search_bar_set_search (self->search_bar, self->search);
  return g_object_new (EXAMPLE_TYPE_DOCUMENT, NULL);
}
