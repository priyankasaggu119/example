#define G_LOG_DOMAIN "ide-editor-search-bar"

#include "example-config.h"

#include <dazzle.h>
#include <glib/gi18n.h>

#include "example-editor-search-bar.h"

struct _ExampleEditorSearchBar
{
  DzlBin                   parent_instance;

  DzlSignalGroup          *search_signals;
  DzlBindingGroup         *search_bindings;
  GtkSearchEntry          *search;

  GObject                 *search_entry_tag;

  GtkCheckButton          *case_sensitive;
  GtkButton               *replace_all_button;
  GtkButton               *replace_button;
  GtkSearchEntry          *replace_entry;
  GtkEntry                *search_entry;
  GtkGrid                 *search_options;
  GtkCheckButton          *use_regex;
  GtkCheckButton          *whole_word;
  GtkLabel                *search_text_error;

  guint                    match_source;

  guint                    show_options : 1;
  guint                    replace_mode : 1;
};

enum {
  PROP_0,
  PROP_REPLACE_MODE,
  PROP_SHOW_OPTIONS,
  N_PROPS
};

enum {
  STOP_SEARCH,
  N_SIGNALS
};

G_DEFINE_TYPE (ExampleEditorSearchBar, example_editor_search_bar, DZL_TYPE_BIN)

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];


static void
search_entry_populate_popup (ExampleEditorSearchBar *self,
                             GtkWidget          *widget,
                             GtkEntry     *entry)
{
  g_assert (EXAMPLE_IS_EDITOR_SEARCH_BAR (self));
  g_assert (GTK_IS_MENU (widget));
  g_assert (GTK_IS_ENTRY (entry));

  if (GTK_IS_MENU (widget))
    {
      g_autoptr(DzlPropertiesGroup) group = NULL;

      GtkWidget *item;
      GtkWidget *sep;
      guint pos = 0;

      item = gtk_check_menu_item_new_with_label (_("Regular "));
      gtk_actionable_set_action_name (GTK_ACTIONABLE (item), "search-settings.regex-enabled");
      gtk_menu_shell_insert (GTK_MENU_SHELL (widget), item, pos++);
      gtk_widget_show (item);

      item = gtk_check_menu_item_new_with_label (_("Case sensitive"));
      gtk_actionable_set_action_name (GTK_ACTIONABLE (item), "search-settings.case-sensitive");
      gtk_menu_shell_insert (GTK_MENU_SHELL (widget), item, pos++);
      gtk_widget_show (item);

      item = gtk_check_menu_item_new_with_label (_("Match whole word only"));
      gtk_actionable_set_action_name (GTK_ACTIONABLE (item), "search-settings.at-word-boundaries");
      gtk_menu_shell_insert (GTK_MENU_SHELL (widget), item, pos++);
      gtk_widget_show (item);

      sep = gtk_separator_menu_item_new ();
      gtk_menu_shell_insert (GTK_MENU_SHELL (widget), sep, pos++);
      gtk_widget_show (sep);

      if (self->search != NULL)
        {
          group = dzl_properties_group_new (G_OBJECT (self->search));
          dzl_properties_group_add_all_properties (group);
        }

      gtk_widget_insert_action_group (widget, "search-settings", G_ACTION_GROUP (group));
    }
}



static void
example_editor_search_bar_class_init (ExampleEditorSearchBarClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/example/ui/example-editor-search-bar.ui");
   gtk_widget_class_bind_template_child (widget_class, ExampleEditorSearchBar, case_sensitive);
  gtk_widget_class_bind_template_child (widget_class, ExampleEditorSearchBar, replace_all_button);
  gtk_widget_class_bind_template_child (widget_class, ExampleEditorSearchBar, replace_button);
  gtk_widget_class_bind_template_child (widget_class, ExampleEditorSearchBar, replace_entry);
  gtk_widget_class_bind_template_child (widget_class, ExampleEditorSearchBar, search_entry);
  gtk_widget_class_bind_template_child (widget_class, ExampleEditorSearchBar, search_options);
  gtk_widget_class_bind_template_child (widget_class, ExampleEditorSearchBar, search_text_error);
  gtk_widget_class_bind_template_child (widget_class, ExampleEditorSearchBar, use_regex);
  gtk_widget_class_bind_template_child (widget_class, ExampleEditorSearchBar, whole_word);

   gtk_widget_class_set_css_name (widget_class, "exampleeditorsearchbar");
}

static void
example_editor_search_bar_init (ExampleEditorSearchBar *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

   self->search_signals = dzl_signal_group_new (GTK_TYPE_SEARCH_ENTRY);

  self->search_bindings = dzl_binding_group_new ();

  dzl_binding_group_bind (self->search_bindings, "regex-enabled",
                          self->use_regex, "active",
                          G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  dzl_binding_group_bind (self->search_bindings, "case-sensitive",
                          self->case_sensitive, "active",
                          G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

dzl_binding_group_bind (self->search_bindings, "at-word-boundaries",
                          self->whole_word, "active",
                          G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  g_signal_connect_swapped (self->search_entry,
                            "populate-popup",
                            G_CALLBACK (search_entry_populate_popup),
                            self);

}

gboolean
example_editor_search_bar_get_show_options (ExampleEditorSearchBar *self)
{
  g_return_val_if_fail (EXAMPLE_IS_EDITOR_SEARCH_BAR (self), FALSE);

  return self->show_options;
}

void
example_editor_search_bar_set_show_options (ExampleEditorSearchBar *self,
                                        gboolean            show_options)
{
  g_return_if_fail (EXAMPLE_IS_EDITOR_SEARCH_BAR (self));

  show_options = !!show_options;

  if (self->show_options != show_options)
    {
      self->show_options = show_options;
      gtk_widget_set_visible (GTK_WIDGET (self->search_options), show_options);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SHOW_OPTIONS]);
    }
}

GtkSearchEntry *
example_editor_search_bar_get_search (ExampleEditorSearchBar *self)
{
  g_return_val_if_fail (EXAMPLE_IS_EDITOR_SEARCH_BAR (self), NULL);

  return self->search;
}

void
example_editor_search_bar_set_search (ExampleEditorSearchBar *self,
                                  GtkSearchEntry    *search)
{
  g_return_if_fail (EXAMPLE_IS_EDITOR_SEARCH_BAR (self));

  if (g_set_object (&self->search, search))
    {
      dzl_signal_group_set_target (self->search_signals, search);
      dzl_binding_group_set_source (self->search_bindings, search);
    }
}


